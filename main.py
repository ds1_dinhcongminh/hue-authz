import databases
import sqlalchemy
import os
import logging

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from fastapi import FastAPI
from dotenv import load_dotenv
from typing import Callable

from config.constants import open_api_group_tags
from models.user_model import user_table
from models.resource_model import resource_table
from models.role_model import role_table
from models.permission_model import permission_table
from models.casbin_rule_model import casbin_rule_table
from routes import resource_routers, user_routers, role_routers, permission_routers, casbin_rule_routers

_logger = logging.getLogger(__name__)

load_dotenv()

'''
    DATABASE initialization
'''
database = databases.Database(os.environ.get("DB_URL"))
metadata = sqlalchemy.MetaData() #ORM
engine = sqlalchemy.create_engine(
    os.environ.get("DB_URL")
)
user_table.metadata.create_all(engine)
resource_table.metadata.create_all(engine)
role_table.metadata.create_all(engine)
permission_table.metadata.create_all(engine)
casbin_rule_table.metadata.create_all(engine)

#########################################
'''
    Handlers for start/shutdown events
'''
def create_start_app_handler(app: FastAPI) -> Callable:    
    async def start_app() -> None:
        _logger.info("Connecting to DB...")
        await database.connect()
        _logger.info("DB Connected")
    return start_app

def create_stop_app_handler(app: FastAPI) -> Callable:
    async def stop_app() -> None:
        _logger.info("Disconnecting to DB...")
        await database.disconnect()
        _logger.info("DB disconnected")
    return stop_app
########################################
'''
    app initialization + config
'''
app = FastAPI(
    swagger_ui_parameters={"docExpansion":"none"},
    
)

origins = [
    "http://localhost"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"]
)


app.include_router(user_routers.router)
app.include_router(resource_routers.router)
app.include_router(role_routers.router)
app.include_router(permission_routers.router)
app.include_router(casbin_rule_routers.router)

app.add_event_handler("startup",create_start_app_handler(app))
app.add_event_handler("shutdown",create_stop_app_handler(app))