
from models.user_model import User, UserCreation, user_table

class UserServices():
    
    def __init__(self, database):
        self.database = database        
    
    async def list_users(self, limit: int, offset: int):
        query = user_table.select().limit(limit).offset(offset)
        await self.database.connect()
        return await self.database.fetch_all(query)


    async def create_user(self, user_object: UserCreation):
        query = user_table.insert().values(user_object.dict())
        await self.database.connect()
        last_user_id = await self.database.execute(query) # async ??
        return {
            **user_object.dict(),
            "id": last_user_id
        }