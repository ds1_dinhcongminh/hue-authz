from models.permission_model import Permission, PermissionCreationSchema, PermissionUpdateByIdSchema, permission_table
from sqlalchemy import update

class PermissionServices():

    def __init__(self, database):
        self.database = database

    async def list_resources(self, limit, offset):
        query = permission_table.select().limit(limit).offset(offset)
        await self.database.connect()
        return await self.database.fetch_all(query)    
    
    async def create_permission(self, permission_object: PermissionCreationSchema):
        query = permission_table.insert().values(permission_object.dict())
        await self.database.connect()
        last_permission_id = await self.database.execute(query)
        return {
            **permission_object.dict(),
            "id":last_permission_id
        }

    async def update_permission_by_id(
        self, 
        id: int, 
        permission_object: PermissionUpdateByIdSchema):
        
        # statement = (
        #     update(permission_table).
        #     where(permission_table.id == id).
        #     values(permission_object)
        # )
        await self.database.connect()
        query = permission_table.update(
            whereclause=(permission_table.c.id == id),
            values=permission_object.dict()
        )
        
        updated_permission_id = await self.database.execute(query)
        return {
            **permission_object.dict(),
            "updated_permission_id":updated_permission_id
        }