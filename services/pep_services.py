from distutils.command.config import config
import casbin

e = casbin.Enforcer(
    config = "../config/basic_casbin_model.conf",
    policy = "../config/basic_casbin_policy.csv"
    )