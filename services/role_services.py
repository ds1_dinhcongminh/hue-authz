from models.role_model import Role, RoleCreation, role_table

class RoleServices():
    def __init__(self, database):
        self.database = database

    async def list_roles(self, limit: int, offset: int):
        query = role_table.select().limit(limit).offset(offset)
        await self.database.connect()
        return await self.database.fetch_all(query)
    
    async def create_role(self, role_object: RoleCreation):
        query = role_table.create().value(role_object.dict())
        await self.database.connect()
        last_role_id = await self.database.execute(query)
        return {
            **role_object.dict(),
            "id":last_role_id
        }