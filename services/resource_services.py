from models.resource_model import Resource, ResourceCreation, resource_table

class ResourceServices():

    def __init__(self, database):
        self.database = database

    async def list_resources(self,limit: int, offset: int):
        query = resource_table.select().limit(limit).offset(offset)
        await self.database.connect()
        return await self.database.fetch_all(query)

    async def create_resource(self, resource_object: ResourceCreation):
        query = resource_table.insert().values(resource_object.dict())
        await self.database.connect()
        last_resource_id = await self.database.execute(query)
        return {
            **resource_object.dict(),
            "id": last_resource_id
        }

