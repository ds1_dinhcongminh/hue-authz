from models.casbin_rule_model import CasbinRule, CasbinRuleCreation, CasbinRuleUpdate, casbin_rule_table

class CasbinRuleServices():

    def __init__(self, database):
        self.database = database

    async def list_casbin_rules(self, limit: int, offset: int):
        query = casbin_rule_table.select().limit(limit).offset(offset)
        await self.database.connect()
        return await self.database.fetch_all(query)

    async def create_casbin_rule(self, casbin_rule_object: CasbinRuleCreation):
        query = casbin_rule_object.insert().values(casbin_rule_object.dict())
        await self.database.connect()
        last_casbin_rule_id = await self.database.execute(query)
        return {
            **casbin_rule_object.dict(),
            "id": last_casbin_rule_id
        }
    
    # async def update_casbin_rule(self, casbin_rule_id: str, casbin_rule_object:CasbinRuleUpdate):
        

