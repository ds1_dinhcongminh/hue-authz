import resource
from typing import List
from dotenv import load_dotenv
from fastapi import APIRouter, Path
import databases
import os

from dotenv import load_dotenv

from models.role_model import Role, RoleCreation
from services.role_services import RoleServices

load_dotenv()
database = databases.Database(os.environ.get('DB_URL'))
role_services = RoleServices(database)

router = APIRouter(tags=['role'])

@router.get("/roles/{limit}/{offset}",response_model=List[Role])
async def list_roles_pagination(
    limit: int = Path(...,title='Limit',gt=0),
    offset: int = Path(...,title='Offset',gt=0)
):
    return await role_services.list_roles(limit,offset)

@router.post("/roles",response_model=Role)
async def create_role(role_object: RoleCreation):
    return await role_services.create_role(role_object)