import databases
import os

from typing import List
from fastapi import APIRouter, Path
from dotenv import load_dotenv

from models.permission_model import Permission, PermissionCreationSchema, PermissionUpdateByIdSchema
from services.permission_services import PermissionServices

load_dotenv()

database = databases.Database(os.environ.get('DB_URL'))

permission_services = PermissionServices(database)

router = APIRouter(tags=['permission'])

@router.get("/permissions/{limit}/{offset}",response_model=List[Permission])
async def list_permissions_pagination(
    limit: int = Path(...,title='Limit',gt=0),
    offset: int = Path(...,title='Offset',ge=0)
):    
    return await permission_services.list_resources(limit, offset)

@router.post("/permissions",response_model=PermissionCreationSchema)
async def create_permission(permission_object: PermissionCreationSchema):
    return await permission_services.create_permission(permission_object)

@router.patch("/permissions/{id}", response_model = PermissionUpdateByIdSchema)    
async def update_permission_by_id(
    permission_object: PermissionUpdateByIdSchema,
    id: int = Path(...,title="Id")
):
    return await permission_services.update_permission_by_id(id, permission_object)