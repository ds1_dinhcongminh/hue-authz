from typing import List
from fastapi import APIRouter, Path

import databases
import os

from dotenv import load_dotenv
from models.casbin_rule_model import CasbinRule, CasbinRuleCreation

from services.casbin_rule_services import CasbinRuleServices


load_dotenv()
database = databases.Database(os.environ.get('DB_URL'))

casbin_rule_services = CasbinRuleServices(database)

router = APIRouter(tags=['casbin-rule'])

@router.get("/casbin-rule/{limit}/{offset}",response_model=List[CasbinRule])
async def list_casbin_rules(
    limit: int = Path(...,title='Limit',gt=0),
    offset: int = Path(...,title='Offset',ge=0)
):
    return await casbin_rule_services.list_casbin_rules(limit,offset)

@router.post("/casbin-rule",response_model=CasbinRule)
async def create_casbin_rule(casbin_rule_object: CasbinRuleCreation):
    return await casbin_rule_services.create_casbin_rule(casbin_rule_object)