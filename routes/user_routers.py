import databases
import os

from typing import List
from fastapi import APIRouter, Path
from dotenv import load_dotenv

from models.user_model import User, UserCreation
from services.user_services import UserServices

load_dotenv()

database = databases.Database(os.environ.get('DB_URL'))

user_services = UserServices(database)

router = APIRouter(tags=['user'])

@router.get("/users/{limit}/{offset}",response_model=List[User])
async def list_users_pagination(
    limit: int = Path(...,title='Limit',gt=0),
    offset: int = Path(...,title='Offset',ge=0)
):    
    return await user_services.list_users(limit, offset)

@router.post("/users/",response_model=User)
async def create_user(user_object: UserCreation):
    return await user_services.create_user(user_object)