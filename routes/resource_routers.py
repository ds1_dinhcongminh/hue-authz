import databases
import os

from typing import List
from fastapi import APIRouter, Path
from dotenv import load_dotenv

from models.resource_model import Resource, ResourceCreation
from services.resource_services import ResourceServices

load_dotenv()

database = databases.Database(os.environ.get('DB_URL'))

resource_services = ResourceServices(database)

router = APIRouter(tags=['resource'])

@router.get("/resources/{limit}/{offset}",response_model=List[Resource])
async def list_resources(
    limit: int = Path(...,title='Limit',gt=0),
    offset: int = Path(...,title='Offset',ge=0)
):    
    return await resource_services.list_resources(limit,offset)

@router.post("/resources",response_model=Resource)
async def create_resource(resource_object: ResourceCreation):
    return await resource_services.create_resource(resource_object)