from datetime import date
from importlib.metadata import metadata
from typing import List
import sqlalchemy


from pydantic import BaseModel


class Role(BaseModel):
    id:             int
    uuid:           str
    owner:          List[str]
    name:           str
    users:          List[str]
    roles:          List[str]
    permissions:    List[str]
    is_enabled:     bool
    created_time:   date
    created_by:     str


class RoleCreation(BaseModel):
    owner:          List[str]
    name:           str
    users:          List[str]
    roles:          List[str]
    permissions:    List[str]
    is_enabled:     bool


metadata = sqlalchemy.MetaData()

role_table = sqlalchemy.Table(
    "role",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("uuid", sqlalchemy.String()),

    sqlalchemy.Column("owner", sqlalchemy.String()),
    sqlalchemy.Column("name", sqlalchemy.String()),

    sqlalchemy.Column("users", sqlalchemy.ARRAY(sqlalchemy.String())),
    sqlalchemy.Column("roles", sqlalchemy.ARRAY(sqlalchemy.String())),
    sqlalchemy.Column("permissions", sqlalchemy.ARRAY(sqlalchemy.String())),
    sqlalchemy.Column("is_enabled", sqlalchemy.Boolean),
    sqlalchemy.Column("created_time", sqlalchemy.Date()),
    sqlalchemy.Column("created_by", sqlalchemy.String()),    
    sqlalchemy.Column("updated_at", sqlalchemy.Date())
)
