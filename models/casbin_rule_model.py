from pydantic import BaseModel

import sqlalchemy

class CasbinRule(BaseModel):
    id:         int
    uuid:       str
    p:          str
    v0:         str
    v1:         str
    v2:         str
    v3:         str
    v4:         str
    v5:         str

    

class CasbinRuleCreation(BaseModel):
    id:         int
    uuid:       str
    p:          str
    v0:         str
    v1:         str
    v2:         str
    v3:         str
    v4:         str
    v5:         str

class CasbinRuleUpdate(BaseModel):
    id:         int
    uuid:       str
    p:          str
    v0:         str
    v1:         str
    v2:         str
    v3:         str
    v4:         str
    v5:         str

metadata = sqlalchemy.MetaData()

casbin_rule_table = sqlalchemy.Table(
    "casbin_rule",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("uuid", sqlalchemy.String()),
    sqlalchemy.Column("p", sqlalchemy.String()),
    sqlalchemy.Column("v0", sqlalchemy.String()),
    sqlalchemy.Column("v1", sqlalchemy.String()),
    sqlalchemy.Column("v2", sqlalchemy.String()),
    sqlalchemy.Column("v3", sqlalchemy.String()),
    sqlalchemy.Column("v4", sqlalchemy.String()),
    sqlalchemy.Column("v5", sqlalchemy.String()),
    sqlalchemy.Column("created_time", sqlalchemy.Date()),
    sqlalchemy.Column("created_by", sqlalchemy.String()),
    sqlalchemy.Column("updated_at", sqlalchemy.Date())
)