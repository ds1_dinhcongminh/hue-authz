from faulthandler import is_enabled
from re import M
from pydantic import BaseModel
from typing import List

import sqlalchemy


class Permission(BaseModel):
    id:             str
    uuid:           str
    owner:          str
    name:           str
    users:          List[str]
    roles:          List[str]
    resource_type:  str
    resources:      List[str]
    actions:        List[str]
    constraints:    List[str]
    effect:         str
    is_enabled:     bool


class PermissionCreationSchema(BaseModel):
    uuid:           str
    owner:          str
    name:           str
    users:          List[str]
    roles:          List[str]
    resource_type:  str
    resources:      List[str]
    actions:        List[str]
    constraints:    List[str]
    effect:         str
    is_enabled:     bool


class PermissionUpdateByIdSchema(BaseModel):    
    name:           str
    users:          List[str]
    roles:          List[str]
    resource_type:  str
    resources:      List[str]
    actions:        List[str]
    constraints:    List[str]
    effect:         str
    is_enabled:     bool

metadata = sqlalchemy.MetaData()


permission_table = sqlalchemy.Table(
    "permission",
    metadata,
    sqlalchemy.Column("id",sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("uuid", sqlalchemy.String()),

    sqlalchemy.Column("owner", sqlalchemy.String()),
    sqlalchemy.Column("name", sqlalchemy.String()),
    sqlalchemy.Column("users", sqlalchemy.ARRAY(sqlalchemy.String())),
    sqlalchemy.Column("roles", sqlalchemy.ARRAY(sqlalchemy.String())),
    sqlalchemy.Column("resource_type",sqlalchemy.String()),
    sqlalchemy.Column("resources",sqlalchemy.ARRAY(sqlalchemy.String())),
    sqlalchemy.Column("actions",sqlalchemy.ARRAY(sqlalchemy.String())),
    sqlalchemy.Column("constraints",sqlalchemy.ARRAY(sqlalchemy.String())),
    sqlalchemy.Column("effect", sqlalchemy.String()),
    sqlalchemy.Column("is_enabled", sqlalchemy.Boolean),

    sqlalchemy.Column("created_time", sqlalchemy.Date()),
    sqlalchemy.Column("created_by", sqlalchemy.String()),
    sqlalchemy.Column("updated_at", sqlalchemy.Date())
)
