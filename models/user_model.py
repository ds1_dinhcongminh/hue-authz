import sqlalchemy

from pydantic import BaseModel

class User(BaseModel):
    id:         int
    uuid:       str
    full_name:  str
    email:      str
    phone:      str

class UserCreation(BaseModel):
    # id: int
    uuid:       str
    full_name:  str
    email:      str
    phone:      str


metadata = sqlalchemy.MetaData()


user_table = sqlalchemy.Table(
    "users",
    metadata,
    sqlalchemy.Column("id",sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("full_name",sqlalchemy.String(100)),
    sqlalchemy.Column("email",sqlalchemy.String(100)),
    sqlalchemy.Column("phone",sqlalchemy.String(100))    
)

