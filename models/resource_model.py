import json
from typing import List
import sqlalchemy

from pydantic import BaseModel


class Resource(BaseModel):
    id:         int

    uuid:       str
    owner:      List[str] = []
    provider:   List[str] = []
    name:       str
    url:        str
    path:       str
    parent:     List[str] = []


class ResourceCreation(BaseModel):

    uuid:       str
    owner:      List[str] = []
    provider:   List[str] = []
    name:       str
    url:        str
    path:       str
    parent:     List[str] = []


metadata = sqlalchemy.MetaData()


resource_table = sqlalchemy.Table(
    "resource",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("uuid", sqlalchemy.String()),
    sqlalchemy.Column("owner", sqlalchemy.ARRAY(sqlalchemy.String())),
    sqlalchemy.Column("provider", sqlalchemy.ARRAY(sqlalchemy.String())),
    sqlalchemy.Column("name", sqlalchemy.String()),
    sqlalchemy.Column("url", sqlalchemy.String()),
    sqlalchemy.Column("path", sqlalchemy.String()),
    sqlalchemy.Column("parent", sqlalchemy.ARRAY(sqlalchemy.String())),

    sqlalchemy.Column("created_time", sqlalchemy.Date()),
    sqlalchemy.Column("created_by", sqlalchemy.String()),
    sqlalchemy.Column("updated_at", sqlalchemy.Date())
)
