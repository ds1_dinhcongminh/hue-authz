"""init 2

Revision ID: 10bd711f2e43
Revises: 597de674a54f
Create Date: 2022-04-13 15:44:28.393290

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '10bd711f2e43'
down_revision = '597de674a54f'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
