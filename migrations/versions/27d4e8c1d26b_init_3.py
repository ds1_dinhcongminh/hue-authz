"""init 3

Revision ID: 27d4e8c1d26b
Revises: 8ab3d99b161c
Create Date: 2022-04-13 15:48:14.501458

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '27d4e8c1d26b'
down_revision = '8ab3d99b161c'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
