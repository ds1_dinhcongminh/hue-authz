"""init 2

Revision ID: 597de674a54f
Revises: b6069956749d
Create Date: 2022-04-13 15:39:14.964638

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '597de674a54f'
down_revision = 'b6069956749d'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
