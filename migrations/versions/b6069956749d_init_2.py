"""init 2

Revision ID: b6069956749d
Revises: b192a034dd0e
Create Date: 2022-04-13 15:38:52.794202

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b6069956749d'
down_revision = 'b192a034dd0e'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
