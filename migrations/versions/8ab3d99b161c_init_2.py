"""init 2

Revision ID: 8ab3d99b161c
Revises: 10bd711f2e43
Create Date: 2022-04-13 15:44:55.669901

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8ab3d99b161c'
down_revision = '10bd711f2e43'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
