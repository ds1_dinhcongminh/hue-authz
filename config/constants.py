
open_api_group_tags = [
    {
        "name": "default",
        "description": "default apis",
        "externalDocs": {
            "url": "https://www.google.com"
        }
    },
    {
        "name": "user",
        "description": "Quản lý danh sách ID người dùng",
        "externalDocs": {
            "url": "https://www.google.com"
        }
    },
    {
        "name": "resource",
        "description": "Quản lý danh sách resources",
        "externalDocs": {
            "url": "https://www.google.com"
        }
    },
    {
        "name": "role",
        "description": "Quản lý danh sách role",
        "externalDocs": {
            "url": "https://www.google.com"
        }
    },
    {
        "name": "permission",
        "description": "Quản lý danh sách permission",
        "externalDocs": {
            "url": "https://www.google.com"
        }
    },
    {
        "name":"csbin-rules",
        "description":"Quản lý danh sách Rule theo casbin",
        "externalDocs":{
            "url":"https://www.google.com"
        }
    }
]
